import tables
import csv
# pip install h5py==2.10.0 for DeepCS
import h5py
import json
import numpy as np
import pickle as pk


def load_model_epoch(model, epoch, path):
    model.load(path + 'epoch' + str(epoch) + '_code.h5',
               path + 'epoch' + str(epoch) + '_desc.h5')


def save_model_epoch(model, epoch, path):
    model.save(path + 'epoch' + str(epoch) + '_code.h5',
               path + 'epoch' + str(epoch) + '_desc.h5')


def load_hdf5(vecfile, start_offset=0, chunk_size=-1, bow=False):
    table = tables.open_file(vecfile)
    # if chunk_size == -1:
    data, index = (table.get_node('/phrases')[:], table.get_node('/indices')[:])
    # else:
    # data, index = (table.get_node('/phrases'), table.get_node('/indices'))
    data_len = index.shape[0]

    if chunk_size == -1:
        chunk_size = data_len
    start_offset = start_offset % data_len
    off_set = start_offset

    sents = []
    # m = 0
    while off_set < start_offset + chunk_size:
        if off_set >= data_len:
            chunk_size = start_offset + chunk_size - data_len
            start_offset = 0
            off_set = 0
        length, pos = index[off_set]['length'], index[off_set]['pos']
        off_set += 1
        vec = data[pos:pos + length].astype('int32')
        if bow:
            vec = list(set(vec))
        sents.append(vec)
        # if len(set(vec)) > m:
        #     m = len(set(vec))
        #     print(m)
    table.close()
    return sents


def save_hdf5(path, lines):
    data = []
    index = []
    pos = 0

    for i in range(len(lines)):
        line = lines[i]
        length = len(line)
        data.extend(line)

        index.append((pos, length))
        pos += length

    data = np.array(data)
    dt = np.dtype([('pos', np.int_), ('length', np.int_)])
    index = np.array(index, dtype=dt)

    with h5py.File(path, 'w') as writer:
        writer.create_dataset('phrases', data=data, compression='gzip')
        writer.create_dataset('indices', data=index, compression='gzip')


def load_hdf5_count(vecfile):
    index = tables.open_file(vecfile).get_node('/indices')
    return index.shape[0]


def save_code_reprs(path, vecs):
    npvecs = vecs
    fvec = tables.open_file(path, 'w')
    atom = tables.Atom.from_dtype(npvecs.dtype)
    filters = tables.Filters(complib='blosc', complevel=9)
    ds = fvec.create_carray(fvec.root, 'vecs', atom, npvecs.shape, filters=filters)
    ds[:] = npvecs
    fvec.close()


def load_code_reprs(path):
    return tables.open_file(path, 'r').get_node('/vecs')[:]


def pad(data, len=None):
    from keras.preprocessing.sequence import pad_sequences
    return pad_sequences(data, maxlen=len, padding='post', truncating='post', value=0)


def convert(vocab, words):
    if type(words) == str:
        words = words.strip().lower().split(' ')
    return [vocab.get(w, 0) for w in words]


def revert(vocab, indices):
    ivocab = dict((v, k) for k, v in vocab.items())
    return [ivocab.get(i, 'UNK') for i in indices]


def load_csv(path):
    reader = csv.reader(open(path, 'r', encoding='utf-8'), delimiter=',')
    lines = []
    for r in reader:
        lines.append(r)
    return lines


def save_csv(path, lines):
    writer = csv.writer(open(path, 'w', encoding='utf-8', newline=''), delimiter=',')
    writer.writerows(lines)


def load_txt(path):
    lines = open(path, 'r', encoding='utf-8').readlines()
    for i in range(len(lines)):
        lines[i] = lines[i][:-1]
    return lines


def save_txt(path, lines):
    with open(path, 'w', encoding='utf-8') as file:
        for line in lines:
            file.write(line + '\n')


def write(path, lines):
    with open(path, 'w', encoding='utf-8') as file:
        for line in lines:
            file.write(line + '\n')


def save_pkl(path, data):
    pk.dump(data, open(path, 'wb'))


def load_pkl(path):
    return pk.load(open(path, 'rb'))


def encode_str(line):
    return np.array([int.from_bytes(i.encode('utf-8'), byteorder='little') for i in line])


def decode_str(line):
    return ''.join([int(i).to_bytes(byteorder='little', length=1).decode('utf-8') for i in line])


def load_json(path):
    lines = load_txt(path)
    data = []
    for line in lines:
        try:
            data.append(json.loads(line))
        except json.JSONDecodeError as e:
            print(e)
    return data


def save_json(path, data):
    lines = [json.dumps(line) for line in data]
    save_txt(path, lines)
