import util
import sqlite3 as sl
import numpy as np
import hashlib
import threadpool
import threading
import queue
import time
from sklearn.preprocessing import normalize

import time
# conda install faiss-cpu -c pytorch for python 3.7
import faiss
from sklearn.metrics.pairwise import cosine_similarity


def index_code(path_vecs, path_index, count):
    for i in range(count):
        print(i)
        vecs = util.load_code_reprs(path_vecs + 'use.codevecs' + str(i) + '.h5')
        vecs_normalize = normalize(vecs, norm='l2', axis=1)
        index = faiss.IndexFlatIP(500)
        print(index.is_trained)
        index.add(vecs_normalize)
        print(index.ntotal)
        faiss.write_index(index, path_index + 'vecs' + str(i) + '.idx')
        print()


def index_code_all(path_vecs, path_index, count):
    index = faiss.IndexFlatIP(500)
    for i in range(count):
        print(i)
        vecs = util.load_code_reprs(path_vecs + 'use.codevecs' + str(i) + '.h5')
        vecs_normalize = normalize(vecs, norm='l2', axis=1)
        index.add(vecs_normalize)
    faiss.write_index(index, path_index + 'vecs.idx')


def split_rawcode(model, path_rawcode, path_split):
    index = 0
    count = 0
    data = []
    with open(path_rawcode, 'rb') as reader:
        for line in reader:
            print('%d-%d' % (index, count))
            try:
                line = bytes(line).decode('utf-8')
            except UnicodeDecodeError as e:
                line = str(line)
            data.append(line)
            count += 1

            if count == model.chunk_size:
                util.save_pkl(path_split + 'rawcode' + str(index) + '.pkl', data)
                index += 1
                count = 0
                data = []

    util.save_pkl(path_split + 'rawcode' + str(index) + '.pkl', data)


def index_rawcode(path_db, path_split, count):
    con = sl.connect(path_db)
    with con:
        for i in range(count):
            con.execute('CREATE TABLE CODE' + str(i) + ' (id INTEGER, code TEXT);')
            lines = util.load_pkl(path_split + 'rawcode' + str(i) + '.pkl')
            for j in range(len(lines)):
                print('%d-%d' % (i, j))
                con.execute('INSERT INTO CODE' + str(i) + ' (id, code) values (?, ?)', (j, lines[j]))
            con.execute('CREATE INDEX index_code_' + str(i) + ' ON CODE' + str(i) + '(id)')


def search_thread(model, path_query, path_vecs, path_rawcode, count):
    t0 = time.time()
    vocab = util.load_pkl('data/github/vocab.desc.pkl')
    queries = util.load_txt(path_query)
    for idx in range(len(queries)):
        t1 = time.time()
        print(idx)
        tokens = queries[idx].strip().lower().split(' ')
        desc = list(set([vocab.get(token, 0) for token in tokens]))
        desc_pad = util.pad([desc], model.desc_len)
        desc_reprs = model.repr_desc([desc_pad])

        threads = []
        for i in range(count):
            # thread_idx(i, path_rawcode, path_vecs, desc_reprs)
            # t = threading.Thread(target=thread_i, args=(i, path_rawcode, path_vecs, desc_reprs))
            t = threading.Thread(target=thread_idx, args=(i, path_rawcode, path_vecs, desc_reprs))
            threads.append(t)
        for t in threads:
            t.start()
        for t in threads:
            t.join()

        t2 = time.time()
        print(str(idx) + ', period: ' + str(t2 - t1) + ', total: ' + str(t2 - t0))


def thread_i(i, path_rawcode, path_vecs, desc_reprs):
    code = util.load_pkl(path_rawcode + 'rawcode' + str(i) + '.pkl')
    code_reprs = util.load_pkl(path_vecs + 'use.codevecs' + str(i) + '.pkl')
    sim = cosine_similarity(desc_reprs, code_reprs)
    ids = np.argsort(np.negative(sim))[:10]
    # top = [[sim[id], code[id], hashlib.md5(code[id].encode('utf-8')).digest()] for id in ids]
    # print(i)


def thread_idx(i, path_rawcode, path_vecs, desc_reprs):
    code = util.load_pkl(path_rawcode + 'rawcode' + str(i) + '.pkl')
    index = faiss.read_index(path_vecs + str(i) + '.idx')
    sim, ids = index.search(desc_reprs, k=10)
    sim = sim[0]
    ids = ids[0]
    top = [[sim[k], code[ids[k]], hashlib.md5(code[ids[k]].encode('utf-8')).digest()] for k in range(len(ids))]
    # print(i)


def search(model, path_query, path_vecs, path_rawcode, count, path_result):
    vocab = util.load_pkl('data/github/vocab.desc.pkl')
    queries = util.load_txt(path_query)
    desc = []
    for i in range(len(queries)):
        tokens = queries[i].strip().lower().split(' ')
        desc.append(list(set([vocab.get(token, 0) for token in tokens])))
    desc_pad = util.pad(desc, model.desc_len)
    desc_reprs = model.repr_desc([desc_pad])

    results = [[] for i in range(len(queries))]
    for i in range(count):
        print(i)
        code = util.load_pkl(path_rawcode + 'rawcode' + str(i) + '.pkl')
        code_reprs = util.load_code_reprs(path_vecs + 'use.codevecs' + str(i) + '.h5')
        sims = cosine_similarity(desc_reprs, code_reprs)

        for j in range(len(queries)):
            sim = sims[j]
            ids = np.argsort(np.negative(sim))[:10]
            top = [[sim[id], code[id], hashlib.md5(code[id].encode('utf-8')).digest()] for id in ids]
            results[j].extend(top)
    util.save_pkl(path_result, results)


def search_by_index(model, path_query, path_index, path_rawcode, count, path_result):
    vocab = util.load_pkl('data/github/vocab.desc.pkl')
    queries = util.load_txt(path_query)
    desc = []
    for i in range(len(queries)):
        tokens = queries[i].strip().lower().split(' ')
        desc.append(list(set([vocab.get(token, 0) for token in tokens])))
    desc_pad = util.pad(desc, model.desc_len)
    desc_reprs = model.repr_desc([desc_pad])
    desc_reprs = normalize(desc_reprs, norm='l2', axis=1)

    results = [[] for i in range(len(queries))]
    for i in range(count):
        print(i)
        code = util.load_pkl(path_rawcode + 'rawcode' + str(i) + '.pkl')
        code_index = faiss.read_index(path_index + 'vecs' + str(i) + '.idx')
        D, I = code_index.search(desc_reprs, k=10)

        for j in range(len(queries)):
            sim = D[j]
            ids = I[j]
            top = [[sim[k], code[ids[k]], hashlib.md5(code[ids[k]].encode('utf-8')).digest()] for k in range(len(ids))]
            results[j].extend(top)
    util.save_pkl(path_result, results)


def ranking(path_result, path_query, path_output):
    results = util.load_pkl(path_result)
    queries = util.load_txt(path_query)
    with open(path_output, 'w', encoding='utf-8') as writer:
        for i in range(len(queries)):
            top = sorted(results[i], reverse=True, key=lambda x: x[0])

            writer.write(str(i + 1) + '. ' + queries[i] + '\n')
            pre_sim = []
            pre_hash = []
            for j in range(len(top)):
                # for j in range(10):
                if len(pre_sim) == 10:
                    break
                if j > 0:
                    j_sim = top[j][0]
                    j_hash = top[j][2]
                    if abs(min(np.array(pre_sim) - j_sim)) < 0.01 or j_hash in pre_hash:
                        continue
                pre_sim.append(top[j][0])
                pre_hash.append(top[j][2])
                writer.write(str(len(pre_sim)) + '. ' + str(top[j][0]) + '. ' + top[j][1] + '\n')
            writer.write('\n')
            writer.flush()


if __name__ == '__main__':
    # for i in range(166):
    #     print(i)
    #     code_reprs = util.load_code_reprs('output/deepcs_new/vecs/use.codevecs' + str(i) + '.h5')
    #     util.save_pkl('output/deepcs_new/vecs_pkl/use.codevecs' + str(i) + '.pkl', code_reprs)
    # for i in range(167):
    i = 166
    print(i)
    vecs = util.load_code_reprs('output/unif_new/vecs/use.codevecs' + str(i) + '.h5')
    vecs_normalize = normalize(vecs, norm='l2', axis=1)
    index = faiss.IndexFlatIP(500)
    index.add(vecs_normalize)
    faiss.write_index(index, 'output/unif_new/index/' + str(i) + '.idx')
