import time
import searcher
import util
from model_unif import UNIF

if __name__ == '__main__':
    # t0 = time.time()
    model = UNIF()
    model.build()
    model.compile()

    """------github------"""
    # workdir = 'output/unif_github/'
    path_query = 'data/queries174.txt'
    fpkl = 'result174.pkl'
    ftxt = 'result174.txt'

    """984s"""
    # util.load_model_epoch(model, 500, workdir + 'models/')
    # searcher.search(model, path_query, workdir + 'vecs/', 'rawcode/github/', 163, workdir + fpkl)
    # searcher.ranking(workdir + fpkl, path_query, workdir + ftxt)

    """------new------ 4098s for 9 queries => 455s/queries"""
    workdir = 'output/unif_new/'
    util.load_model_epoch(model, 500, 'output/unif_github/models/')
    searcher.search_thread(model, path_query, workdir + 'index/', 'rawcode/new/', 166)
    # searcher.search(model, path_query, workdir + 'vecs/', 'rawcode/new/', 166, workdir + fpkl)
    # searcher.ranking(workdir + fpkl, path_query, workdir + ftxt)

    """------new-tune------"""
    # workdir = 'output/unif_new_tune/'
    # util.load_model_epoch(model, 1000, workdir + 'models/')
    # searcher.search(model, path_query, workdir + 'vecs/', 'rawcode/new/', 166, workdir + fpkl)
    # searcher.ranking(workdir + fpkl, path_query, workdir + ftxt)

    # t1 = time.time()
    # print(t1 - t0)
