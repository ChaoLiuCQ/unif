import util
import time
import random
import numpy as np

# pip install tensorflow-gpu==2.0.1 keras==2.3.1 for cuda=10.0 and NVIDIA GTX 1060
from keras.engine import Input
from keras.models import Model
from keras.layers import Embedding, Dense, Lambda, Dot, dot, Concatenate, Multiply
from keras import backend as K


class UNIF:
    def __init__(self):
        self.save_every = 10
        self.chunk_size = 100000
        self.batch_size = 128

        self.methname_len = 30
        self.apiseq_len = 40
        self.desc_len = 96

        self._code_repr_model = None
        self._desc_repr_model = None
        self._sim_model = None
        self._training_model = None

        self.methname_ini_embed = util.load_pkl('data/embedding.methname.pkl')
        self.apiseq_ini_embed = util.load_pkl('data/embedding.apiseq.pkl')
        self.desc_ini_embed = util.load_pkl('data/embedding.desc.pkl')
        # self.methname_ini_embed = []
        # self.apiseq_ini_embed = []
        # self.desc_ini_embed = []

        self.n_words = 10000
        self.n_embed = 500
        # self.embedding[0] = np.zeros(self.n_embed)

        self.methname = Input(shape=(self.methname_len,), dtype='int32')
        self.apiseq = Input(shape=(self.apiseq_len,), dtype='int32')
        self.desc_good = Input(shape=(self.desc_len,), dtype='int32')
        self.desc_bad = Input(shape=(self.desc_len,), dtype='int32')

    def build(self):
        """building code model"""
        methname = Input(shape=(self.methname_len,), dtype='int32')
        methname_embedding = Embedding(input_dim=self.n_words, output_dim=self.n_embed,
                                       weights=[np.array(self.methname_ini_embed)],
                                       mask_zero=True, trainable=True)(methname)

        apiseq = Input(shape=(self.apiseq_len,), dtype='int32')
        apiseq_embedding = Embedding(input_dim=self.n_words, output_dim=self.n_embed,
                                     weights=[np.array(self.apiseq_ini_embed)],
                                     mask_zero=True, trainable=True)(apiseq)

        code_embedding = Concatenate(axis=1)([methname_embedding, apiseq_embedding])

        layer_attn_input = Lambda(lambda x: K.permute_dimensions(x, pattern=(0, 2, 1)))(code_embedding)
        layer_attn = Dense(1, use_bias=False, activation=None)(layer_attn_input)

        weights_product = dot([code_embedding, layer_attn], axes=[2, 1])
        weights_exp = Lambda(lambda x: K.exp(x) * K.cast_to_floatx(K.not_equal(x, 0.)))(weights_product)
        weights_softmax = Lambda(lambda x: K.squeeze(x / K.sum(x), axis=2))(weights_exp)

        code_repr = dot([code_embedding, weights_softmax], axes=[1, 1])
        self._code_repr_model = Model(inputs=[methname, apiseq], outputs=[code_repr])

        # self._code_repr_model.compile(loss='cosine_proximity', optimizer='adam')
        # chunk_methname = util.load_hdf5_old('data/github/train.methname.h5', 0, self.chunk_size, bow=True)
        # chunk_apiseq = util.load_hdf5_old('data/github/train.apiseq.h5', 0, self.chunk_size, bow=True)
        # pad_methname = util.pad(chunk_methname, self.methname_len)
        # pad_apiseq = util.pad(chunk_apiseq, self.apiseq_len)
        # self._code_repr_model.fit([pad_methname, pad_apiseq], epochs=1, batch_size=self.batch_size)
        # print()

        """building description model"""
        desc = Input(shape=(self.desc_len,), dtype='int32')
        desc_embedding = Embedding(input_dim=self.n_words, output_dim=self.n_embed,
                                   weights=[np.array(self.desc_ini_embed)],
                                   mask_zero=True, trainable=True)(desc)

        weights_count_re = Lambda(lambda x: 1 / K.sum(K.cast_to_floatx(K.not_equal(x, 0)), axis=1))(desc)
        weights_sum = Lambda(lambda x: K.sum(x, axis=1))(desc_embedding)
        weights_average = Multiply()([weights_sum, weights_count_re])

        self._desc_repr_model = Model(inputs=[desc], outputs=[weights_average])

        # chunk_desc = util.load_hdf5_old('data/github/train.desc.h5', 0, 2)
        # pad_desc = util.pad(chunk_desc, self.desc_len)
        # value = self._desc_repr_model.predict([pad_desc])
        # print()

        """building similarity model"""
        code_repr = self._code_repr_model([methname, apiseq])
        desc_repr = self._desc_repr_model([desc])
        cos_sim = Dot(axes=1, normalize=True)([code_repr, desc_repr])

        self._sim_model = Model(inputs=[methname, apiseq, desc], outputs=[cos_sim])

        """building training model"""
        good_sim = self._sim_model([self.methname, self.apiseq, self.desc_good])
        bad_sim = self._sim_model([self.methname, self.apiseq, self.desc_bad])
        loss = Lambda(lambda x: K.maximum(1e-6, 0.05 - x[0] + x[1]), output_shape=lambda x: x[0])([good_sim, bad_sim])
        self._training_model = Model(inputs=[self.methname, self.apiseq, self.desc_good, self.desc_bad], outputs=[loss])

    def compile(self, **kwargs):
        self._code_repr_model.compile(loss='cosine_proximity', optimizer='adam', **kwargs)
        self._desc_repr_model.compile(loss='cosine_proximity', optimizer='adam', **kwargs)
        self._training_model.compile(loss=lambda y_true, y_pred: y_pred + y_true - y_true, optimizer='adam', **kwargs)
        self._sim_model.compile(loss='binary_crossentropy', optimizer='adam', **kwargs)

    def fit(self, x, **kwargs):
        y = np.zeros(shape=x[0].shape[:1], dtype=np.float32)
        return self._training_model.fit(x, y, **kwargs)

    def repr_code(self, x, **kwargs):
        return self._code_repr_model.predict(x, **kwargs)

    def repr_desc(self, x, **kwargs):
        return self._desc_repr_model.predict(x, **kwargs)

    def predict(self, x, **kwargs):
        return self._sim_model.predict(x, **kwargs)

    def save(self, code_model_file, desc_model_file, **kwargs):
        self._code_repr_model.save_weights(code_model_file, **kwargs)
        self._desc_repr_model.save_weights(desc_model_file, **kwargs)

    def load(self, code_model_file, desc_model_file, **kwargs):
        self._code_repr_model.load_weights(code_model_file, **kwargs)
        self._desc_repr_model.load_weights(desc_model_file, **kwargs)


def train(model, reload_epoch, total_epoch, path_model, path_train):
    if reload_epoch > 0:
        util.load_model_epoch(model, reload_epoch, path_model)

    for epoch in range(reload_epoch + 1, total_epoch + 1):
        print('Epoch %d :: \n' % epoch, end='')

        start_offset = (epoch - 1) * model.chunk_size
        chunk_methname = util.load_hdf5(path_train + 'train.methname.h5', start_offset, model.chunk_size, bow=True)
        chunk_apiseq = util.load_hdf5(path_train + 'train.apiseq.h5', start_offset, model.chunk_size, bow=True)
        chunk_desc = util.load_hdf5(path_train + 'train.desc.h5', start_offset, model.chunk_size, bow=True)
        chunk_bad_desc = [desc for desc in chunk_desc]
        random.shuffle(chunk_bad_desc)

        pad_methname = util.pad(chunk_methname, model.methname_len)
        pad_apiseq = util.pad(chunk_apiseq, model.apiseq_len)
        pad_good_desc = util.pad(chunk_desc, model.desc_len)
        pad_bad_desc = util.pad(chunk_bad_desc, model.desc_len)

        model.fit([pad_methname, pad_apiseq, pad_good_desc, pad_bad_desc],
                  epochs=1, batch_size=model.batch_size)

        if epoch % model.save_every == 0:
            util.save_model_epoch(model, epoch, path_model)


def repr_code(model, reload_epoch, path_model, path_use, path_vecs):
    util.load_model_epoch(model, reload_epoch, path_model)
    total = util.load_hdf5_count(path_use + 'use.methname.h5')
    count = int(total / model.chunk_size) + 1

    for i in range(count):

        start_offset = i * model.chunk_size
        chunk_size = model.chunk_size
        if i == count - 1:
            chunk_size = total - i * model.chunk_size

        print('%d-%d-%d' % (i, start_offset, chunk_size))

        methname = util.load_hdf5(path_use + 'use.methname.h5', start_offset, chunk_size, bow=True)
        apiseq = util.load_hdf5(path_use + 'use.apiseq.h5', start_offset, chunk_size, bow=True)
        pad_methname = util.pad(methname, model.methname_len)
        pad_apiseq = util.pad(apiseq, model.apiseq_len)

        vecs = model.repr_code([pad_methname, pad_apiseq], batch_size=1000).astype('float32')
        util.save_code_reprs(path_vecs + 'use.codevecs' + str(i) + '.h5', vecs)


if __name__ == '__main__':
    t0 = time.time()
    model = UNIF()
    model.build()
    model.compile()

    """Training: 14899s = 4.13h, loss = 2.6879e-04"""
    # train(model, reload_epoch=0, total_epoch=500, path_model='output/unif_github/models/',
    #       path_train='data/github/')
    """1517s"""
    # repr_code(model, reload_epoch=500, path_model='output/unif_github/models/',
    #           path_use='data/github/', path_vecs='output/unif_github/vecs/')

    """1541s"""
    # repr_code(model, reload_epoch=500, path_model='output/unif_github/models/',
    #           path_use='data/new/', path_vecs='output/unif_new/vecs/')

    """Fine-Tuning"""
    # train(model, reload_epoch=500, total_epoch=1000, path_model='output/unif_new_tune/models/',
    #       path_train='data/new/')
    # repr_code(model, reload_epoch=1000, path_model='output/unif_new_tune/models/',
    #           path_use='data/new/', path_vecs='output/unif_new_tune/vecs/')

    t1 = time.time()
    print(t1 - t0)
